#1 - Medidor de Distancia
Diseñar e implementar un sistema, basado en el hardware provisto por la cátedra, que
permita medir la distancia del sistema a una pared.
Requerimientos:
a. El sistema debe realizar mediciones de distancia cada 1 segundo.
b. Si la distancia es inferior a 1 metro debe emitir tres sonidos consecutivos de
600 ms de duración en total.
c. Si la distancia es inferior a 1 m debe encender un led rojo.
d. Si la distancia es superior a 2 m debe encender un led verde.
e. Si la distancia se encuentra entre 1 my 2 m debe encender un led amarillo.
f. Si la distancia es inferior a 2 m el sistema debe transmitirla, expresada en
cm, a través de la UART a una PC a una tasa de transferencia de 9600
baudios. El valor de distancia deberá ser convertido a ascii para poder ser
visualizado por un terminal.
