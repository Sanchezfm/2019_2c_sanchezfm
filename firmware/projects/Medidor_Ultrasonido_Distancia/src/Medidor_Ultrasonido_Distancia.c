/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 2-10-19: Versión inicial
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Medidor_Ultrasonido_Distancia.h"
#include "systemclock.h"
#include "switch.h"
#include "hc_sr4.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "DisplayITS_E0803.h"
/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0
#define DECIMAL 10

/*banderas globales */
	uint16_t ESTADOTECLA1=0; /* ESTADOTECLA1= ESTADO DE LA TECLA 1*/
	uint16_t MEDICIONCENTIMETROS=1;/* BANDERAS PARA SABER SI MIDO EN CM O IN */
	uint16_t MEDICIONPULGADAS=0;
	uint16_t HOLD=0; /* VARIABLE PARA SABER SI MIDO CONTINUAMENTE O PAUSO LA MEDIDA */
	uint16_t  sensor_distancia=0;
	timer_config timmer;
	serial_config puerto={SERIAL_PORT_PC,115200,NO_INT};

/*==================[internal data definition]===============================*/
/*==================[internal functions declaration]=========================*/
/*==================[external data definition]===============================*/

   void ptr_int_func1(void){
	   ESTADOTECLA1=!ESTADOTECLA1;
						}
	void ptr_int_func2(void){
		HOLD=!HOLD;
					    }
	void ptr_int_func3(void){
		MEDICIONCENTIMETROS=!MEDICIONCENTIMETROS;
		MEDICIONPULGADAS=!MEDICIONPULGADAS;
							}
	void ptr_int_func4(void){

		 MEDICIONPULGADAS=!MEDICIONPULGADAS;
		 MEDICIONCENTIMETROS=!MEDICIONCENTIMETROS;
	}

void ptr_int_display_timmer(void){


    	UartSendString(SERIAL_PORT_PC, UartItoa(sensor_distancia, DECIMAL));
    	UartSendBuffer(SERIAL_PORT_PC, " cm \n\r", strlen(" cm \n\r"));
   	   	   	   	   	   	   	   	  }

/*==================[external functions declaration]==========================*/


int main(void)
{
	/*DECLARACIONES*/

	gpio_t pines[7]= {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5}; /* Vector que configura el usuario y se lo pasa al driver display*/

	 /*INICIALIZAICIONES*/

	ITSE0803Init(pines);
	HcSr04Init(T_FIL2, T_FIL3); /*​T_FIL2 para inicializar el echo y T_FIL3 para el trigger */
	SystemClockInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, ptr_int_func1);
	SwitchActivInt(SWITCH_2, ptr_int_func2);
	SwitchActivInt(SWITCH_3, ptr_int_func3);
	SwitchActivInt(SWITCH_4, ptr_int_func4);

	UartInit(&puerto);


   timmer.timer=TIMER_B;  /*configuramos el timmer B con un periodo de 1000 */
   timmer.period=1000; /* EL PERIODO SERA DE 1 SEGUNDO */
   timmer.pFunc=ptr_int_display_timmer;
   TimerInit(&timmer);
   TimerStart(TIMER_B); /*arrancamos el timmer b */




    while(1)
    {
    	if((ESTADOTECLA1==ON)&(HOLD==OFF)){

    		    				sensor_distancia=HcSr04ReadDistanceCentimeters();


    		    		    	 if((MEDICIONCENTIMETROS==1)&(MEDICIONPULGADAS==0)){
    		    		    		 	 	 	 	   sensor_distancia=HcSr04ReadDistanceCentimeters();}

    		    		    	 if((MEDICIONPULGADAS==1)&(MEDICIONCENTIMETROS==0)){
    		    		    		                   sensor_distancia=HcSr04ReadDistanceInches();}



    		    		    	 ITSE0803DisplayValue(sensor_distancia);
    		}


    		    	 if(ESTADOTECLA1==OFF){
    		    		    	 ITSE0803DisplayValue(OFF);
    		    		    	 }

    		        if(HOLD==ON){
    		        		     ITSE0803DisplayValue(sensor_distancia);

    		        }
    }

}





/*==================[end of file]============================================*/
