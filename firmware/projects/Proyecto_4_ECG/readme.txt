PROYECTO_4_ECG
Este proyecto permite tomar una señal de ECG y calcular la frecuencia cardiaca de la persona (FC) utilizano el valor inverso del periodo QRS 
(FC INSTANTANEA) de dicha señal, luego dependiendo del valor de esta frecuencia prende un led rojo (taquicardia) u azul (bradicardia), ademas trasmite 
el valor de dicha FC mediante bluehtooh a la computadora.
Para realizarse esta aplicación se utilizaron los siguientes drivers:

# "systemclock.h"
# "timer.h"
# "uart.h" 
# "string.h"
# "DisplayITS_E0803.h" 
# "analog_io.h"
# "Delay.h"
# "led.h"
# "bt_sr05.h+.h"

EL PASAJE DE DATOS SE TRASMITIRA EN EL SIEGUIENTE FORMATO:
EL VALOR EN DE LA FC EN DECIMAL Y SU UNIDAD LAT/MIN