Requerimientos:
1. Realizar la adquisición de la señal analógica de presión a una frecuencia de 100 Hz,
por tramos de un segundo, de manera cíclica.
La relación entre presión y voltaje a la entrada del CAD es:
50mmHg -> 0V
125mmHg -> 3.3V
2. La adquisición se debe disparar al recibir un flanco descendente por el GPIO7 del
conector de expansión 1 de la EDU-CIAA.
3. Se debe presentar en el LCD (ITS_E0803) el valor máximo de presión de cada
segundo adquirido.
4. Enviar a través de la SERIAL_PORT_PC a un terminal el valor máximo encontrado
cada segundo con el siguiente formato:
120 mmHg
122 mmHg
.
.
.
5. El sistema debe inicializar en estado apagado (led Rojo encendido) y, al presionar la
tecla 1, debe comenzar la adquisición y el reporte de datos (encendiendo el led Verde y
apagando el Rojo). Al presionar la tecla 2 el sistema debe volver al estado apagado.
V1.0 (