Diseñar e implementar un sistema, basado en el hardware provisto por la cátedra, que
permita adquirir una señal y calcular su valor promedio por segundo. El valor calculado
deberá ser enviado a través de un puerto serie a la PC.
Especificaciones:
● Frecuencia de muestreo: 100 Hz.
● El sistema deberá togglear un GPIO cada vez que se adquiera una nueva muestra
de la señal de entrada.
● Velocidad de transferencia: 57600 Baudios.
● El sistema deberá togglear el led azul cada vez que se envíe un nuevo valor
promedio por la UART.