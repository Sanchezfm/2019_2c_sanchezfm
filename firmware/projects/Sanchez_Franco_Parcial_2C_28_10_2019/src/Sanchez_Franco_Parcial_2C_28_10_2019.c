/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 * * SF 	Sánchez, Franco

 *
 *
 *
 * Revisión:
 * 28-10-19: Versión inicial

 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "../inc/Sanchez_Franco_Parcial_2C_28_10_2019.h"
#include "switch.h"
#include "analog_io.h"
#include "DisplayITS_E0803.h"


/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0
#define BAUDRATE 115200 /* Velocidad de tranferencia de datos para UART*/
#define DECIMAL 10
/*Frecuencia de muestreo 250 ->> en 1 s tenemos 250 muestras */
/* tiempo de adquisicion 1s ->> TIEMPO QUE SE ADQUIERE LA SEÑAL EN SEGUNDOS ->> PARA CALCULAR LA CANTIDAD DE MUESTRAS*/

/*==================[internal data definition]===============================*/

int b=0;
uint16_t VALOR=0; /*Variable donde se guarda la conversion del AD*/
uint16_t  PRESIONES[100]; /* VECTOR QUE GUARDAD LAS PRESIONES CADA 4 ms*/
uint16_t AUX=0; /*VARIABLE A UTILIZAR PARA ENCONTRAR LA MAXIMAN Y MINIMA PRESION ARTERIAL */
float AUX_PROM=0; /*VARIABLE A UTILIZAR PARA ENCONTRAR EL PROMEDIO DE LAS PRESIONES ARTERIALES EN 1 SEG */
uint16_t MAXIMO=0; /*VARIABLE A GUARDAR LA MAXIMA PRESION EN 1 SEGUNDO*/
uint16_t MINIMO=0; /*VARIBALE A GUARDAR LA MINIMA PRESION*/
float PROMEDIO=0; /*VARIABLE A GUARDAR EL PROMEDIO DE LAS PRESIONES ADQUIRIDAS EN 1 SEG*/
uint16_t CANT_MUESTRAS=250; /*DEPENDE DE LA FREC MUESTREO Y EL TIEMPO DE ADQUISICION*/

/* PARA RELIZAR LA CONVERSION EL VALOR CONVERTIDO SE MULTIPLICA POR EL DELTA DE PRESIONES Y SE DIVIDE POR 1024 (2 a la n, con n num de bits en este caso 10 -> EDUCIAA)*/
uint16_t DELTA=200; /* REPRESENTA EL VALOR DE 200 mmhg . 0 mmHg */

analog_input_config AD_conversor;
timer_config timmerA; /* Para adquirir datos en 1 seg ->> trabaja a 4 ms*/
serial_config puerto={SERIAL_PORT_PC,BAUDRATE,NO_INT};
uint16_t VALOR_MAX_BINARIO=1024; /* ES 2 A LA N, CON N NUM DE BITS, EN ESTE CASO 10 -> LA EDU CIAA, PARA CONVERTIR EL VALOR DE LA PRESION*/

/*Banderas a utilizar en el main y en las interrupciones de las conversiones*/
uint8_t DATOS_CALCULADOS=0;
uint8_t FIN_CONVERSION=0;

/*==================[internal functions declaration]=========================*/

uint16_t maximo (void) /* funcion que opera con el vector PRESIONES (global) y calcula el maximo de todas las presiones en 1 SEG la retorna por la variable AUX*/
{
		int i;
		AUX=PRESIONES[0]; /* asigno a la variable AUX la primer temperatura para comparar*/
		for(i=1;i<CANT_MUESTRAS;i++)
		{
			if(PRESIONES[i]>AUX)
				{
				AUX=PRESIONES[i];
				}

		}

return(AUX);
}

uint16_t minimo (void) /* funcion que opera con el vector PRESIONES (global) y calcula el minimo de todas las presiones en 1 SEG y la retorna por la variable AUX*/
{
		int i;
		AUX=PRESIONES[0]; /* asigno a la variable AUX la primer temperatura para comparar*/

		for(i=1;i<CANT_MUESTRAS;i++)
		{
			if(PRESIONES[i]<AUX)
				{
				AUX=PRESIONES[i];
				}

		}
return(AUX);
}

float prom(void)  /* FUNCION QUE ENVIA EL PROMEDIO DE LAS PRESIONES A TRAVES DE LA VARIABLE AUX*/
{
	int a;
	AUX=0;
	for(a=0;a<CANT_MUESTRAS;a++)
	{
		AUX=AUX+PRESIONES[a];
	}
AUX_PROM=AUX/CANT_MUESTRAS;
return(AUX_PROM);
}

/*==================[external data definition]===============================*/



/* El hecho de que las interrupciones no dependan de la bandera de los datos calculados permite que siempre que se toquen se mostrara los valores del ultimo segundo*/

void ptr_int_tecla_1_maximo(void)  /* se apreta la tecla 1 por lo que se envia por la uart el maximo, se muestra en el display y se prende el led rojo */
{

		ITSE0803DisplayValue(MAXIMO);
	    UartSendString(SERIAL_PORT_PC, UartItoa(MAXIMO, DECIMAL));
	    UartSendBuffer(SERIAL_PORT_PC," mmHg \n\r", strlen(" mmHg \n\r")); /* FORMATO EN EL QUE SE ENVIA LA MAXIMA PRESION ARTERIAL*/
        LedOn(LED_1);  /*Prendo el led rojo y apago los demas*/
	    LedOff(LED_2);
	    LedOff(LED_3);


}

void ptr_int_tecla_2_minimo(void)  /*se apreta la tecla 2 con lo que se muestra el minimo y se en via por la uart ademas se prende el led amarillo*/
{


	    ITSE0803DisplayValue(MINIMO);
		UartSendString(SERIAL_PORT_PC, UartItoa(MINIMO, DECIMAL));
	    UartSendBuffer(SERIAL_PORT_PC," mmHg \n\r", strlen(" mmHg \n\r")); /* FORMATO EN EL QUE SE ENVIA LA MAXIMA PRESION ARTERIAL*/
		LedOn(LED_2);  /*prendo el led amarillo y apago los demas*/
		LedOff(LED_1);
		LedOff(LED_3);


}

void ptr_int_tecla_3_promedio(void)  /*se apreta la tecla 3 por lo que se envia por la uart el promedio de las presiones, se lo muestra por display y se prende el led verde*/
{
		 ITSE0803DisplayValue(PROMEDIO);
	   	 UartSendString(SERIAL_PORT_PC, UartItoa(PROMEDIO, DECIMAL));
		 UartSendBuffer(SERIAL_PORT_PC," mmHg \n\r", strlen(" mmHg \n\r")); /* FORMATO EN EL QUE SE ENVIA LA MAXIMA PRESION ARTERIAL*/
		 LedOn(LED_3);  /*prendo el led verde y los demas*/
		 LedOff(LED_1);
		 LedOff(LED_2);




}

void ptr_int_conversion_on(void)
{
	AnalogStartConvertion();   /*ARRANCO A CONVERTIR CADA 4 ms */
	FIN_CONVERSION=ON;     /* SUBO LA BANDERA DE FINALIZACION DE LA CONVERSION*/

}

void ptr_int_ad_conversor_read(void)   /* ESTA INTERRUPCION GUARDA LA CONVERSION EN VALOR Y LA ALMACENA EN EL VECTOR PRESIONES, CUANDO SE LLEGA A LAS 100 MUESTRAS
 	 	 	 	 	 	 	 	 	 REALIZA LOS CALCULOS DE MAXIMO, MINIMO Y PROMEDIO*/
{if(FIN_CONVERSION==ON)
	{
	AnalogInputRead(CH1, &VALOR);
	PRESIONES[b]=VALOR;
	b++;
	DATOS_CALCULADOS=OFF; /* BAJO LA BANDERA DE LOS DATOS CALCULADOS SIEMPRE QUE NO HAYA DATOS EN 1 SEG*/
	if(b==CANT_MUESTRAS)
		{
		b=0;
		MAXIMO=maximo();
		MAXIMO=((MAXIMO*DELTA)/VALOR_MAX_BINARIO);
		MINIMO=minimo();
		MINIMO=((MINIMO*DELTA)/VALOR_MAX_BINARIO);
		PROMEDIO=prom();
		PROMEDIO=((PROMEDIO*DELTA)/VALOR_MAX_BINARIO);
		ITSE0803DisplayValue(MAXIMO); /*por defecto siempre se muestra la maxima presion arterial en 1 seg*/
		DATOS_CALCULADOS=ON;
		}
	FIN_CONVERSION=OFF; /*SE BAJA LA BANDERA DE FIN CONVERSION*/

	}
}



/*==================[external functions declaration]==========================*/




int main(void)
{
/*DECLARACIONES*/
	AD_conversor.input=CH1; /*SE CONFIGURA EL CH1 PARA RECIBIR LA SEÑAL A CONVERTIR en el canal 1*/
	AD_conversor.mode=AINPUTS_SINGLE_READ;
	AD_conversor.pAnalogInput=ptr_int_ad_conversor_read;
	gpio_t pines[7]= {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5}; /* Vector que configura el usuario y se lo pasa al driver display*/

/*INICIALIZAICIONES*/
   ITSE0803Init(pines);
   SystemClockInit();
   LedsInit();

   SwitchesInit();
   SwitchActivInt(SWITCH_1, ptr_int_tecla_1_maximo);
   SwitchActivInt(SWITCH_2, ptr_int_tecla_2_minimo);
   SwitchActivInt(SWITCH_3, ptr_int_tecla_3_promedio);

   timmerA.timer=TIMER_A;
   timmerA.period=4;     /*configuramos el timmer A con un periodo de 4 ms PARA QUE ADQUIERA LA SEÑAL, INVERSA DE LA FRECUENCIA DE MUESTREO */
   timmerA.pFunc=ptr_int_conversion_on;
   TimerInit(&timmerA);   /*Inicializamos el timmer A*/
   TimerStart(TIMER_A);	  /* Arrancamos el timmer A*/


   AnalogInputInit(&AD_conversor);
   AnalogOutputInit();



    while(1)
    {	if(DATOS_CALCULADOS==ON) /*solo cada 1 segundo se prenderan los leds que indicaran la maxima presion*/
    	{
    			if(MAXIMO>150)   			  /* Si la maxima presion en 1 seg es mayor a 150 mmHG prendo el led rojo y apago los demas*/
    			{
    				LedOn(LED_RGB_R);
    				LedOff(LED_RGB_B);
    				LedOff(LED_RGB_G);
    			}
    			if((MAXIMO<150)&&(MAXIMO<50)) /* Si la maxima presion en 1 seg es menor a 150 mmHg y mayor a 50 mmHg prendo el led azul y apago los demas*/
    			{
    				LedOn(LED_RGB_B);
    				LedOff(LED_RGB_G);
    			    LedOff(LED_RGB_R);
    			}
    			if(MAXIMO<50) 					/* Si la maxima presion en 1 seg es menor a 50 mmHG prendo el led verde y apago los demas*/
    			{
    				LedOn(LED_RGB_G);
    				LedOff(LED_RGB_B);
    			    LedOff(LED_RGB_R);
    			}
    	}
    }

}





/*==================[end of file]============================================*/
