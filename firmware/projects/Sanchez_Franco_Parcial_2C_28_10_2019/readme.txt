Aplicacion Para medir la presion arterial
Esta aplicacion permite tomar las presiones arteriales de una persona por 1 segundo, luego si no se apreta ninguna tecla mostrara cada 1 segundo
la maxima presion en dicho segundo y encendera un led segun:

La maxima presion es mayor a 150 mmHg ->> enciende el led rojo del RGB y apaga los demas
La maxima presion es menor a 150 mmHG y mayor a 50 mmHg ->> enciende el led azul del RGB y apaga los demas
La maxima presion es menor a 50 mmHg ->> enciende el led verde del RGB y apaga los demas.

Estas acciones antes dichas se encuentran en el programa principal.

El hecho de que se apaguen los demas leds evita la confucion de las maximas presiones en instantes anteriores, por lo cual el led indicara la maxima presion en el 
ultimo segundo, y ademas se estara mostrando en el display si no se apreto ninguna tecla.

Ahora bien si se apreta una tecla:
Si la tecla es la 1 ->> se muestra la maxima presion en el display, se envia por uart a la pc y se prende el led rojo.
Si la tecla es la 2 ->> se muestra la minima presion en el display, se envia por uart a la pc y se prende el led amarillo.
Si la tecla es la 3 ->> se muestra el promedio de las presiones en 1 seg en el display, se envia por uart a la pc y se prende el led verde.

Estas acciones antes mencionadas asociadas a las teclas se encuentran en las interrupciones de cada tecla y mostraran los valores del ultimo segundo.

Para realizar las todo esto la aplicaciòn posee funciones que permiten encontrar el valor maximo, minimo y promedio de las presiones en 1 seg.