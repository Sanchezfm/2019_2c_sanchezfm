/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Sánchez, Franco
 * 			 Lovatto, Flavip

 *
 *
 *
 * Revisión:
 * 04-09-19: Versión inicial

 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "sensor_distancia_ultrasonido.h"
#include "systemclock.h"
#include "bool.h"
#include "hc_sr4.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
/* definimos variables int tiempo_sensor_centimetros y int tiempo_sensor_pulgadas para conocer los valores que devuelve(en cm y in) las funciones del sensor */
	uint16_t tiempo_sensor_centimetros=0;
	uint16_t tiempo_sensor_pulgadas=0;
	HcSr04Init(T_FIL2, T_FIL3); /* ​T_FIL2 para inicializar el echo y T_FIL3 para el trigger */
	SystemClockInit();
	/*LedsInit();*/
while(1){
	/*tiempo_sensor_centimetros=HcSr04ReadDistanceCentimeters();*/
	tiempo_sensor_pulgadas=HcSr04ReadDistanceInches();
}

}
/*==================[end of file]============================================*/

