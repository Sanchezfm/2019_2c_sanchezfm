/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Lovatto, Flavio
 * 			 Sánchez, Franco
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

union parteb
{ uint32_t bar;
  uint8_t  b1;
  uint8_t  b2;
  uint8_t  b3;
  uint8_t  b4;
};

int main(void)
{ // Parte A //
	uint32_t Var= 0x01020304;
  uint8_t v1;
  uint8_t v2;
  uint8_t v3;
  uint8_t v4;

  uint32_t masc4=0x000000FF;
  uint32_t masc3=0x0000FF00;
  uint32_t masc2=0x00FF0000;
  uint32_t masc1=0xFF000000;

  v4= (Var)&(masc4);
  printf("%d v4 \r\n", v4);
  v3= ((Var)&(masc3))>>(8);
  printf("%d v3 \r\n", v3);
  v2= ((Var)&(masc2))>>(16);
  printf("%d v2 \r\n", v2);
  v1= ((Var)&(masc1))>>(24);
  printf("%d v1 \r\n", v1);

  // Parte B//
  union parteb pb;
  pb.bar=0x01020304;
  uint8_t aux=0x00;
  aux= (pb.bar)&(masc4);
  pb.b1=aux;
  printf("%d b4 \r\n", pb.b4);
  aux= ((pb.bar)&(masc3))>>(8);
  pb.b2=aux;
  printf("%d b3 \r\n", pb.b3);
  aux= ((pb.bar)&(masc2))>>(16);
  pb.b3=aux;
  printf("%d b2 \r\n", pb.b2);
  aux= ((pb.bar)&(masc1))>>(24);
  pb.b4=aux;
  printf("%d b1 \r\n", pb.b1);

  	  	 return 0;
}

/*==================[end of file]============================================*/

